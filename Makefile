default: app-build docker-build docker-push 

app-build:
	CGO_ENABLED=0 go build

docker-build:
	docker build -t bobbae/simplews .

docker-push:
	docker push bobbae/simplews

docker-run:
	docker run --rm --name test111 -d  --network host bobbae/simplews test111 :8081

do-curl:
	curl http://localhost:8081
